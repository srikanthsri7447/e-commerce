const homePageContainerEl = document.getElementById('homePage')
const shopNowBtn = document.getElementById('homeShopNowBtn')
const shoppingPageEl = document.getElementById('shoppingPage')

shopNowBtn.addEventListener('click',startShopping)

function startShopping(){
    homePageContainerEl.style.display = 'none'
    shoppingPageEl.style.display = 'flex'
}